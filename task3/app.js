const figlet = require("figlet");
const userText = process.argv[2];

console.log(userText);

figlet.text(userText,{
    font: 'Fire Font-s',
    horizontalLayout: 'fitted',
    verticalLayout: 'default',

}, function(error, data) {
    if (error)
      console.log(error);
    else
      console.log(data);
});